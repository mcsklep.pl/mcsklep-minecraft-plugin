package pl.mcsklep.minecraft.plugin;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

public class ShopConfiguration
{
    private final FileConfiguration configuration;
    private final File              file;

    private String server;
    private String token;

    public ShopConfiguration(FileConfiguration configuration, File file)
    {
        this.configuration = configuration;
        this.file = file;

        this.configuration.options().copyDefaults(true);
    }

    public void load() throws IOException, InvalidConfigurationException
    {
        if (this.file.exists())
        {
            this.configuration.load(this.file);

            this.server = this.configuration.getString("server");
            this.token = this.configuration.getString("api.token", "api_token");
        }
    }

    public void save() throws IOException
    {
        if (!this.file.exists())
        {
            this.file.getParentFile().mkdirs();
            this.file.createNewFile();
        }
        this.configuration.set("server", this.server);
        this.configuration.set("api.token", this.token);

        this.configuration.save(this.file);
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }
}
