package pl.mcsklep.minecraft.plugin;

import java.util.UUID;

public class ShopUtil
{
    public static UUID parseUUID(String text)
    {
        return text != null && text.length() == 36 ? UUID.fromString(text) : null;
    }

    public static boolean parseBoolean(String text)
    {
        return "true".equalsIgnoreCase(text) || "1".equals(text);
    }
}
