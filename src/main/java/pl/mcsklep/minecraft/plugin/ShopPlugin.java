package pl.mcsklep.minecraft.plugin;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;
import pl.mcsklep.sdk.ShopClient;
import pl.mcsklep.sdk.ShopSdk;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class ShopPlugin
        extends JavaPlugin
{
    private ShopConfiguration configuration;
    private ShopSdk           shopSdk;

    @Override
    public void onEnable()
    {
        this.saveDefaultConfig();

        File configFile = new File(this.getDataFolder(), "config.yml");
        this.configuration = new ShopConfiguration(this.getConfig(), configFile);
        try
        {
            this.configuration.load();
        }
        catch (InvalidConfigurationException | IOException e)
        {
            this.getLogger().log(Level.SEVERE, "Wystąpił błąd podczas wczytywania konfiguracji", e);
            this.getPluginLoader().disablePlugin(this);
            return;
        }

        if (this.configuration.getToken() == null || this.configuration.getToken().isEmpty())
        {
            this.getLogger().severe("Nieprawidłowa wartość `api.token` w konfiguracji");
            this.getPluginLoader().disablePlugin(this);
            return;
        }

        this.shopSdk = new ShopClient(this.configuration.getToken());

        ExecuteActionTask executeTask = new ExecuteActionTask(this.getServer(), this, this.shopSdk,
                this.configuration);

        long time = 60 * 20;
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, executeTask, time, time);
    }

    public ShopConfiguration getConfiguration()
    {
        return configuration;
    }

    public ShopSdk getShopSdk()
    {
        return shopSdk;
    }
}
