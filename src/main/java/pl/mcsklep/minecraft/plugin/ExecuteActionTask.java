package pl.mcsklep.minecraft.plugin;

import org.bukkit.Server;
import org.bukkit.entity.Player;
import pl.mcsklep.sdk.ShopSdk;
import pl.mcsklep.sdk.exception.ShopErrorException;
import pl.mcsklep.sdk.model.response.action.ActionResponse;
import pl.mcsklep.sdk.model.response.action.ActionStatus;
import pl.mcsklep.sdk.model.response.order.OrderResponse;
import pl.mcsklep.sdk.model.response.order.OrderStatus;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

public class ExecuteActionTask
        implements Runnable
{
    private final Server server;

    private final ShopPlugin        plugin;
    private final ShopSdk           shopSdk;
    private final ShopConfiguration configuration;

    public ExecuteActionTask(Server server, ShopPlugin plugin, ShopSdk shopSdk, ShopConfiguration configuration)
    {
        this.server = server;
        this.plugin = plugin;
        this.shopSdk = shopSdk;
        this.configuration = configuration;
    }

    public void executeCommand(String command)
    {
        this.server.getScheduler()
                .runTask(this.plugin, () -> this.server.dispatchCommand(this.server.getConsoleSender(), command));
    }

    public boolean isPlayerOnline(String name)
    {
        Player foundPlayer = this.server.getPlayerExact(name);
        return foundPlayer != null || this.server.getOnlinePlayers()
                .stream()
                .anyMatch(player -> player.getName().equalsIgnoreCase(name));
    }

    public boolean isPlayerOnline(UUID uniqueId)
    {
        return this.server.getPlayer(uniqueId) != null;
    }

    @Override
    public void run()
    {
        try
        {
            List<OrderResponse> orders = this.shopSdk.getOrdersByStatus(OrderStatus.PAYED);
            orders.forEach(this::executeOrder);
        }
        catch (IOException | ShopErrorException e)
        {
            this.plugin.getLogger()
                    .log(Level.WARNING, "Wystąpił błąd podczas pobierania listy wszystkich opłaconych zamówień", e);
        }
    }

    private void executeOrder(OrderResponse order)
    {
        this.plugin.getLogger().info("Próba wykonania zamówienia o id " + order.getId());
        order.getActions()
                .stream()
                .filter(action -> action.getStatus() == ActionStatus.PENDING)
                .filter(action -> action.getType().equals("execute_minecraft_command"))
                .forEach(action -> this.executeAction(order, action));
    }

    private void executeAction(OrderResponse order, ActionResponse action)
    {
        String server = action.getParameter("server");
        String command = action.getParameter("command");
        boolean requireOnline = ShopUtil.parseBoolean(action.getParameter("requireOnline"));
        if (server == null || command == null)
        {
            return;
        }

        String name = order.getVariable("minecraft_nick");
        UUID uniqueId = ShopUtil.parseUUID(order.getVariable("minecraft_uuid"));
        boolean canDetectPlayer = name != null || uniqueId != null;
        if (canDetectPlayer && requireOnline)
        {
            if (uniqueId != null && !this.isPlayerOnline(uniqueId))
            {
                return;
            }
            if (uniqueId == null && !this.isPlayerOnline(name))
            {
                return;
            }
        }
        if (!server.equalsIgnoreCase(this.configuration.getServer()))
        {
            return;
        }
        for (Map.Entry<String, String> entry : order.getVariables().entrySet())
        {
            command = command.replace("%" + entry.getKey() + "%", entry.getValue());
        }
        try
        {
            this.shopSdk.completeAction(action.getId());
            this.plugin.getLogger().info("Wykonywanie polecenia /" + command);
            this.executeCommand(command);
        }
        catch (IOException | ShopErrorException e)
        {
            this.plugin.getLogger()
                    .log(Level.WARNING, "Wystąpił błąd podczas wykonywania akcji o id " + action.getId(), e);
        }
    }
}
